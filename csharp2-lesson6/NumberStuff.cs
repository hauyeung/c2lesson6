﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp2_lesson6
{
    public partial class NumberStuff : Form
    {
        public NumberStuff()
        {
            InitializeComponent();
        }

        private void gobutton_Click(object sender, EventArgs e)
        {
            int startnum = Convert.ToInt32(startnumbox.Text);
            int endnum = Convert.ToInt32(endnumbox.Text);
            int numodd = 0;
            int numeven = 0;
            int sum = startnum;
            for (int x = startnum+1; x <= endnum; x++)
            {
                sum += x;
                if (x % 2 == 0)
                {
                    numeven++;
                }
                else if (x%2 ==1)
                {
                    numodd++;
                }
            }
            sumanswerlabel.Text = Convert.ToString(sum);
            nooddnumanswerlabel.Text = Convert.ToString(numodd);
            noevenanswerlabel.Text = Convert.ToString(numeven);
        }
    }
}
