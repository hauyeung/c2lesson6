﻿namespace csharp2_lesson6
{
    partial class NumberStuff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startnumlabel = new System.Windows.Forms.Label();
            this.endnumlabel = new System.Windows.Forms.Label();
            this.startnumbox = new System.Windows.Forms.TextBox();
            this.endnumbox = new System.Windows.Forms.TextBox();
            this.gobutton = new System.Windows.Forms.Button();
            this.sumanswerlabel = new System.Windows.Forms.Label();
            this.nooddnumanswerlabel = new System.Windows.Forms.Label();
            this.sumlabel = new System.Windows.Forms.Label();
            this.numoddlabel = new System.Windows.Forms.Label();
            this.numevennolabel = new System.Windows.Forms.Label();
            this.noevenanswerlabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // startnumlabel
            // 
            this.startnumlabel.AutoSize = true;
            this.startnumlabel.Location = new System.Drawing.Point(41, 27);
            this.startnumlabel.Name = "startnumlabel";
            this.startnumlabel.Size = new System.Drawing.Size(80, 13);
            this.startnumlabel.TabIndex = 0;
            this.startnumlabel.Text = "Staring Number";
            // 
            // endnumlabel
            // 
            this.endnumlabel.AutoSize = true;
            this.endnumlabel.Location = new System.Drawing.Point(41, 65);
            this.endnumlabel.Name = "endnumlabel";
            this.endnumlabel.Size = new System.Drawing.Size(80, 13);
            this.endnumlabel.TabIndex = 1;
            this.endnumlabel.Text = "Ending Number";
            // 
            // startnumbox
            // 
            this.startnumbox.Location = new System.Drawing.Point(148, 27);
            this.startnumbox.Name = "startnumbox";
            this.startnumbox.Size = new System.Drawing.Size(100, 20);
            this.startnumbox.TabIndex = 2;
            // 
            // endnumbox
            // 
            this.endnumbox.Location = new System.Drawing.Point(148, 65);
            this.endnumbox.Name = "endnumbox";
            this.endnumbox.Size = new System.Drawing.Size(100, 20);
            this.endnumbox.TabIndex = 3;
            // 
            // gobutton
            // 
            this.gobutton.Location = new System.Drawing.Point(148, 102);
            this.gobutton.Name = "gobutton";
            this.gobutton.Size = new System.Drawing.Size(75, 23);
            this.gobutton.TabIndex = 4;
            this.gobutton.Text = "Go";
            this.gobutton.UseVisualStyleBackColor = true;
            this.gobutton.Click += new System.EventHandler(this.gobutton_Click);
            // 
            // sumanswerlabel
            // 
            this.sumanswerlabel.AutoSize = true;
            this.sumanswerlabel.Location = new System.Drawing.Point(184, 153);
            this.sumanswerlabel.Name = "sumanswerlabel";
            this.sumanswerlabel.Size = new System.Drawing.Size(0, 13);
            this.sumanswerlabel.TabIndex = 5;
            // 
            // nooddnumanswerlabel
            // 
            this.nooddnumanswerlabel.AutoSize = true;
            this.nooddnumanswerlabel.Location = new System.Drawing.Point(184, 175);
            this.nooddnumanswerlabel.Name = "nooddnumanswerlabel";
            this.nooddnumanswerlabel.Size = new System.Drawing.Size(0, 13);
            this.nooddnumanswerlabel.TabIndex = 6;
            // 
            // sumlabel
            // 
            this.sumlabel.AutoSize = true;
            this.sumlabel.Location = new System.Drawing.Point(41, 153);
            this.sumlabel.Name = "sumlabel";
            this.sumlabel.Size = new System.Drawing.Size(28, 13);
            this.sumlabel.TabIndex = 8;
            this.sumlabel.Text = "Sum";
            // 
            // numoddlabel
            // 
            this.numoddlabel.AutoSize = true;
            this.numoddlabel.Location = new System.Drawing.Point(41, 175);
            this.numoddlabel.Name = "numoddlabel";
            this.numoddlabel.Size = new System.Drawing.Size(124, 13);
            this.numoddlabel.TabIndex = 9;
            this.numoddlabel.Text = "Number of Odd Numbers";
            // 
            // numevennolabel
            // 
            this.numevennolabel.AutoSize = true;
            this.numevennolabel.Location = new System.Drawing.Point(41, 198);
            this.numevennolabel.Name = "numevennolabel";
            this.numevennolabel.Size = new System.Drawing.Size(129, 13);
            this.numevennolabel.TabIndex = 11;
            this.numevennolabel.Text = "Number of Even Numbers";
            // 
            // noevenanswerlabel
            // 
            this.noevenanswerlabel.AutoSize = true;
            this.noevenanswerlabel.Location = new System.Drawing.Point(184, 198);
            this.noevenanswerlabel.Name = "noevenanswerlabel";
            this.noevenanswerlabel.Size = new System.Drawing.Size(0, 13);
            this.noevenanswerlabel.TabIndex = 12;
            // 
            // NumberStuff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.noevenanswerlabel);
            this.Controls.Add(this.numevennolabel);
            this.Controls.Add(this.numoddlabel);
            this.Controls.Add(this.sumlabel);
            this.Controls.Add(this.nooddnumanswerlabel);
            this.Controls.Add(this.sumanswerlabel);
            this.Controls.Add(this.gobutton);
            this.Controls.Add(this.endnumbox);
            this.Controls.Add(this.startnumbox);
            this.Controls.Add(this.endnumlabel);
            this.Controls.Add(this.startnumlabel);
            this.Name = "NumberStuff";
            this.Text = "NumberStuff";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label startnumlabel;
        private System.Windows.Forms.Label endnumlabel;
        private System.Windows.Forms.TextBox startnumbox;
        private System.Windows.Forms.TextBox endnumbox;
        private System.Windows.Forms.Button gobutton;
        private System.Windows.Forms.Label sumanswerlabel;
        private System.Windows.Forms.Label nooddnumanswerlabel;
        private System.Windows.Forms.Label sumlabel;
        private System.Windows.Forms.Label numoddlabel;
        private System.Windows.Forms.Label numevennolabel;
        private System.Windows.Forms.Label noevenanswerlabel;
    }
}

